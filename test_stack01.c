#include <stdio.h>
#include <stdlib.h>
#include "collection.h"

int stack01_test()
{
    int ADD = 33;
    int REM = 22;
    _STACK *l = stack_new();

    int numbers[32] = {0,1,2,3,4,5,6,7,
                       8,9,10,11,12,13,14,15,16,
                       17,18,19,20,21,22,23,24,
                       25,26,27,28,29,30,31,32};
    int i;
    for(i = 0;i < ADD; i++)
    {
        stack_push(l, &numbers[i]);
        printf("Count : %d\n", l->_count);
    }

    printf("Count : %d\n", l->_count);
    printf("Capacity : %d\n", l->_capacity);

    for(i = 0;i < REM; i++)
    {
        stack_pop(l);
        printf("Count : %d\n", l->_count);
    }

    list_print(l);
    printf("Count : %d\n", l->_count);
    printf("Capacity : %d\n", l->_capacity);

    stack_clear(l);
    list_print(l);
    printf("Count : %d\n", l->_count);
    printf("Capacity : %d\n", l->_capacity);
}
