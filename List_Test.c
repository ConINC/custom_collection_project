#include "collection.h"

#define MAX 30
#define ADD_COUNT 20
#define REM_COUNT 16
int test_list01()
{
    _LIST   *l, *l2;
    int     i;
    int     *arr;

    arr = malloc(sizeof(int)*MAX);

    for(i = 0; i < MAX; i++)
        arr[i] = i;


    l = list_new();
    for(i =0; i < ADD_COUNT; i++)
    {
        list_add(l, arr[i]);
    }

    list_print(l);
    for(i =0; i < REM_COUNT; i++)
    {
        list_remove_at(l, 0);
    }
    list_print(l);


    int list_clear(_LIST *l);

    int cap = list_capacity(l);
    int count = list_count(l);
    int empty = list_empty(l);

    l2 = list_duplicate(l);

    list_print(l2);

    void **new_arr = list_toarray(l2);

    for(i = 0; i < l2->_count; i++)
        printf("%d\n", new_arr[i]);

    return 0;
}


