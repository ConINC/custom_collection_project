#include <stdio.h>
#include <stdlib.h>
#include "collection.h"

int list_test01()
{

    int     ADD = 33;
    int     REM = 22;
    int     numbers[21] = {0,1,2,3,4,5,6,7,8,9,10,11,12,
                      13,14,15,16,17,18,19,20};
    int     i;
    _LIST   *l;

    l = list_new();

    ///Add items
    for(i = 0; i < ADD; i++)
    {
        list_add(l, &numbers[i]);
        printf("Count : %d\n", l->_count);
    }

    ///Print all of the contents
    list_print(l);

    for(i = 0; i < REM; i++)
    {
        list_remove_at(l, 4);
        printf("Count : %d\n", l->_count);
    }


    return 0; //All good :]
}
